# Basketball assists
This article is intended to demonstrate investigating relationships between variables using data visualisation.

I use basketball data from the 2018-2019 NBA season as the example.

## Data
- srcdata/league_hexbin_stats.pickle are pickled dictionary files, primarily as produced by
matplotlib.hexbin.
- srcdata/[TEAMCODE]_hexbin_stats.pickle are similar data for each team.
- srcdata/2018_2019_per100_stats.csv shows teams' per 100 possessions stats from basketball-reference.com
- srcdata/2018_19_NBA_shot_logs.csv includes raw data of shots including location, team, player, shot_made, shot_zone

### Article
Link: https://towardsdatascience.com/how-to-visualize-hidden-relationships-in-data-with-python-analysing-nba-assists-f6e2edc1ab5